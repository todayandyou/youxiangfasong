<?php
/**
 * 二分查找
 * @param Array $arr 待查找的数组
 * @param Int $key 要查找的关键字
 * @return Int
 */
function bin_search(Array $arr,$key)
{
    $high = count($arr);
    if($high <= 0)
        return 0;
    $low = 0;
    while($low <= $high)
    {
        //当前查找区间arr[low..high]非空
        $mid=intval(($low + $high) / 2);
        if($arr[$mid] == $key)
            return $mid; //查找成功返回
        if($arr[$mid] > $key)
            $high = $mid - 1; //继续在arr[low..mid-1]中查找
        else
            $low = $mid + 1; //继续在arr[mid+1..high]中查找
    }
    return 0; //当low>high时表示查找区间为空，查找失败
}
$arr = array(1,2,4,6,10,40,50,80,100,110);
$index = bin_search($arr,80);
echo $index;
echo PHP_EOL;
echo $arr[(string)bin_search($arr,80)];
