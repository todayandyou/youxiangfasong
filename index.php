<?php
$arr = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
function get_index(Array $arr,$num){
    $count = count($arr);
    if($count<=0){
        return false;
    }
    $low = 0;
    while($low <= $count){
        $zhong = intval(($low + $count)/2);
        if($arr[$zhong] == $num){
            return $zhong;
        }elseif($arr[$zhong] < $num){
            $low = $zhong + 1;
        }else{
            $count = $zhong - 1;
        }
    }
}
echo get_index($arr,0);
echo $arr[get_index($arr,0)];